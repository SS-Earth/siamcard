# **SiamCard**
### Functions
**install**

* `cd functions`
* `npm install`
 
**emulators**

* `cd ..` back to main directory (/siamcard)
* `firebase emulators:start --only functions`
 
**deploy**

* `cd ..` back to main directory (/siamcard)
* `firebase deploy --only functions`
***
### Backend
**install**

* `cd backend`
* `npm install`
 
**run**

* `npm start`
 
**deploy**

* `cd ..` back to main directory (/siamcard)
* `firebase deploy --only hosting`