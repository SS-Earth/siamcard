import Helper from "./Helper";

const req_header = {
    "Siamcard-Token": getLoginToken(),
}
const API = {
    post: (url, data, options) => {
        callApi('POST', url, data, options)
    },
    get: (url, data, options) => {
        callApi('GET', url, data, options)
    },
    put: (url, data, options) => {
        callApi('PUT', url, data, options)
    },
    delete: (url, data, options) => {
        callApi('DELETE', url, data, options)
    },
    abort: () => {
        if (window.call) {
            window.call.abort();
        }
    },
}

function callApi(method, url, data, options) {
    window.call = window.$.ajax({
        method: method,
        headers: req_header,
        url: `${window.api_host}${url}`,
        data: data,
        beforeSend: (res) => { if (options && options.beforeSend) { options.beforeSend(res) } },
        success: (res) => { if (options && options.success) { options.success(res) } },
        error: (res) => { if (options && options.error) { options.error(res) } },
        complete: (res) => { if (options && options.complete) { options.complete(res) } },
    })
}

function getLoginToken() {
    return Helper.getCookie('login_token');
}
export default API;