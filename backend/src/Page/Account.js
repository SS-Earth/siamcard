import React from 'react';
import { Link, navigate } from '@reach/router';
import Pagination from './../Component/Pagination';
import Helper from './../Helper';
import API from './../API';
import Swal from 'sweetalert2';
import CreateAdmin from './../Component/CreateAdmin'

export default class Account extends React.Component {
    constructor() {
        super();
        this.state = {
            is_loading: true,
            admins: [],
            pagination: false,
            qs: {
                page: Helper.getParameterByName('page') ? Helper.getParameterByName('page') : 1,
                limit: Helper.getParameterByName('limit') ? parseFloat(Helper.getParameterByName('limit')) : 8,
                keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
            },
        }
    }

    componentDidMount() {
        this.fetch();
    }
    componentWillUnmount () {
        API.abort();
    }
    
    componentDidUpdate(prevProps) {
        if (this.props.location.search !== prevProps.location.search) {
            this.setState({
                is_loading: true,
                // users: [],
                // pagination: false,
                qs: {
                    page: Helper.getParameterByName('page') ? Helper.getParameterByName('page') : 1,
                    limit: Helper.getParameterByName('limit') ? parseFloat(Helper.getParameterByName('limit')) : 10,
                    keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
                },
            }, () => {
                this.fetch();
            })
        }
        if (this.props.location.search !== prevProps.location.search && !this.props.location.search) {
            this.setState({
                is_loading: true,
                // users: [],
                // pagination: false,
                qs: {
                    page: 1,
                    limit: 8,
                    keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
                },
            }, () => {
                this.fetch();
            })
        }
    }

    fetch() {
        let filter = { ...this.state.qs };
        API.get(`/admin`,
            filter,
            {
                success: (res) => {
                    this.setState({
                        admins: res.admins,
                        pagination: res.pagination,
                        is_loading: false
                    }, () => {
                        window.KTMenu.createInstances();
                    })
                },
                error: (res) => {
                    this.setState({
                        is_loading: false,
                    })
                }
            }
        )
    }

    create() {
        Helper.reactToDom(
            window.$('body'),
            <CreateAdmin
                onCreateSuccess={() => {
                    this.setState({
                        is_loading: true
                    }, () => {
                        this.fetch();
                    })
                }}
            />
        )
    }

    delete(id, options) {
        Swal.fire({
            title: 'Loading!',
            html: '<b></b>',
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
            }
        })
        API.delete('/admin', { id: id }, {
            success: (res) => {
                if (res.is_success) {
                    Swal.fire({
                        title: "Success!",
                        text: "Delete Account Success",
                        timer: 3000,
                        icon: 'success',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-light'
                        },
                        buttonsStyling: false,
                        reverseButtons: true
                    })
                    if (options.resetButton) {
                        options.resetButton();
                    }
                    this.setState({
                        is_loading: true
                    }, () => {
                        this.fetch();
                    })
                }
                if (!res.is_success) {
                    Swal.fire({
                        title: "Error!",
                        text: res.message,
                        timer: 3000,
                        icon: 'error',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-light'
                        },
                        buttonsStyling: false,
                        reverseButtons: true
                    })
                    if (options.resetButton) {
                        options.resetButton();
                    }
                }
            },
            error: () => {
                Swal.fire({
                    title: "Error!",
                    text: "Something error please try again",
                    timer: 3000,
                    icon: 'error',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-light'
                    },
                    buttonsStyling: false,
                    reverseButtons: true
                })
                if (options.resetButton) {
                    options.resetButton();
                }
            }
        })
    }

    render() {
        return (
            <>
                <div className="toolbar" id="kt_toolbar">
                    <div id="kt_toolbar_container" className="container-fluid d-flex flex-stack">
                        <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                            data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                            className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                            <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Account</h1>
                            <span className="h-20px border-gray-200 border-start mx-4"></span>
                            <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                                <li className="breadcrumb-item text-muted">
                                    <Link to="/" className="text-muted text-hover-primary">Dashboard</Link>
                                </li>
                                <li className="breadcrumb-item">
                                    <span className="bullet bg-gray-200 w-5px h-2px"></span>
                                </li>
                                <li className="breadcrumb-item text-dark">List</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" className="container-fluid">
                        <div className="card">
                            <div className="card-header border-0 pt-6">
                                <div className="card-title">
                                    <form className="d-flex align-items-center position-relative my-1"
                                        ref={(ref => { this.$search = window.$(ref) })}
                                        onSubmit={(e) => {
                                            e.preventDefault();
                                            let data = Helper.getFormInputObject(this.$search);
                                            // console.log(data)
                                            let url = Helper.getQueryStringLinkFromObject(data)
                                            navigate(url);
                                        }}
                                    >
                                        <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black"></rect>
                                                <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black"></path>
                                            </svg>
                                        </span>
                                        <input
                                            type="text"
                                            data-kt-customer-table-filter="search"
                                            className="form-control form-control-solid w-250px ps-15"
                                            placeholder="Search By Name, Email"
                                            name="keyword"
                                            defaultValue={this.state.qs.keyword}
                                        />
                                        <button type="submit" style={{ display: 'none' }}>Search</button>
                                    </form>
                                </div>
                                <div className="card-tools">
                                    <button className="btn btn-primary" onClick={() => {
                                        this.create();
                                    }}>Create</button>
                                </div>
                            </div>
                            <div className="card-body pt-0">
                                <div id="kt_customers_table_wrapper" className="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div className="table-responsive">
                                        <table className="table align-middle table-row-dashed gs-0 gy-4 no-footer" id="kt_customers_table" role="grid">
                                            <thead>
                                                <tr className="fw-bolder text-muted bg-light">
                                                    <th className="ps-4 rounded-start min-w-125px" style={{ verticalAlign: 'middle' }}>Name</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody className="fw-bold text-gray-600" style={{ position: 'relative' }}>
                                                {
                                                    this.state.admins.length > 0 ?
                                                        this.state.admins.map((admin, i) => {
                                                            return (
                                                                <tr key={i}>
                                                                    <td className="ps-4 ">
                                                                        <div className="d-flex flex-column">
                                                                            <div className="text-dark fs-6 fw-bolder">{admin.name}</div>
                                                                            <span className="text-muted fw-bold">{admin.email}</span>
                                                                        </div>
                                                                    </td>

                                                                    <td className="text-end">

                                                                        {/* <Link className="btn btn-icon btn-active-light-primary w-30px h-30px me-3" to={`/account/${admin.email}`} >
                                                                            <span className="svg-icon svg-icon-3">
                                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                                                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                                                                </svg>
                                                                            </span>
                                                                        </Link> */}
                                                                        {
                                                                            !admin.is_admin ?
                                                                                <button
                                                                                    ref={(ref) => {
                                                                                        window.$(ref).off().on('click', () => {
                                                                                            Swal.fire({
                                                                                                title: 'Do you want to delete this account?',
                                                                                                icon: 'question',
                                                                                                showCancelButton: true,
                                                                                                confirmButtonText: 'Confirm',
                                                                                                cancelButtonText: `Cancel`,
                                                                                                customClass: {
                                                                                                    confirmButton: 'btn btn-primary',
                                                                                                    cancelButton: 'btn btn-light'
                                                                                                },
                                                                                                buttonsStyling: false,
                                                                                                reverseButtons: true,
                                                                                                showLoaderOnConfirm: true,
                                                                                            }).then((result) => {
                                                                                                /* Read more about isConfirmed, isDenied below */
                                                                                                if (result.isConfirmed) {
                                                                                                    Helper.setButtonLoading(window.$(ref))
                                                                                                    this.delete(admin.id, {
                                                                                                        resetButton: () => {
                                                                                                            Helper.setButtonLoaded(window.$(ref))
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            })
                                                                                        })
                                                                                    }}
                                                                                    type="button"
                                                                                    className="btn btn-icon btn-active-light-primary w-30px h-30px me-3"
                                                                                >
                                                                                    <span className="indicator-label">
                                                                                        <span className="svg-icon svg-icon-3">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                                                <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black" />
                                                                                                <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black" />
                                                                                                <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black" />
                                                                                            </svg>
                                                                                        </span>
                                                                                    </span>
                                                                                    <span className="indicator-progress"><span className="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                                                                </button>
                                                                                : null
                                                                        }
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })
                                                        :
                                                        !this.state.is_loading ?
                                                            <tr>
                                                                <td colSpan="7" align="center">No Data</td>
                                                            </tr>
                                                            : null
                                                }
                                                {
                                                    this.state.is_loading
                                                        && this.state.admins.length === 0 ?
                                                        <tr>
                                                            <td colSpan="7" align="center"><span className="spinner-border spinner-border-md align-middle"></span></td>
                                                        </tr>
                                                        : null
                                                }
                                                {
                                                    this.state.is_loading
                                                        && this.state.admins.length > 0 ?
                                                        <div className="absolute_loading" >
                                                            <span className="spinner-border spinner-border-md align-middle"></span>
                                                        </div>
                                                        : null
                                                }
                                            </tbody>
                                        </table>

                                    </div>

                                    {
                                        this.state.admins.length > 0
                                            && this.state.pagination ?
                                            <Pagination
                                                pagination={this.state.pagination}
                                                onChangePage={(page) => {
                                                    let qs = this.state.qs;
                                                    qs.page = page;
                                                    let url = Helper.getQueryStringLinkFromObject(qs);
                                                    navigate(url);
                                                }}
                                            />
                                            : null
                                    }


                                </div>
                            </div>
                        </div>
                    </div>
                </div >

            </>
        );
    }
}