import React from 'react';
import { Link, navigate } from '@reach/router';
import Pagination from './../Component/Pagination';
import Helper from './../Helper';
import API from './../API';
import Swal from 'sweetalert2';
const limit = 10;
export default class User extends React.Component {
    constructor() {
        super();
        this.state = {
            is_loading: true,
            is_loading_history: true,
            is_loading_logs: true,
            user: false,
            history: [],
            logs: [],
            qs: {
                page: Helper.getParameterByName('page') ? Helper.getParameterByName('page') : 1,
                limit: Helper.getParameterByName('limit') ? parseFloat(Helper.getParameterByName('limit')) : limit,
                keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
            },
        }
    }


    componentDidMount() {
        this.fetch();
    }
    componentDidUpdate(prevProps) {
        if (this.props.location.search !== prevProps.location.search) {
            this.setState({
                is_loading_logs: true,
                // users: [],
                // pagination: false,
                qs: {
                    page: Helper.getParameterByName('page') ? Helper.getParameterByName('page') : 1,
                    limit: Helper.getParameterByName('limit') ? parseFloat(Helper.getParameterByName('limit')) : limit,
                    keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
                },
            }, () => {
                this.fetchLogs();
            })
        }
        if (this.props.location.search !== prevProps.location.search && !this.props.location.search) {
            this.setState({
                is_loading_logs: true,
                // users: [],
                // pagination: false,
                qs: {
                    page: 1,
                    limit: 8,
                    keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
                },
            }, () => {
                this.fetchLogs();
            })
        }
    }

    async fetch() {

        API.get('/user', { user_id: this.props.uid, ...this.state.qs }, {
            success: (res) => {
                if (res.is_success && res.user) {
                    this.setState({
                        is_loading: false,
                        is_loading_logs: false,
                        user: res.user,
                        logs: res.logs,
                        pagination: res.pagination
                    }, () => {
                        console.log(this.state)
                        window.KTMenu.createInstances();
                    })
                }
            },
            error: (res) => {
                this.setState({
                    is_loading: false,
                })
            }
        })
    }


    async fetchLogs() {
        let filter = { ...this.state.qs, keyword: this.props.uid };
        this.call = API.get(`/logs`,
            filter,
            {
                success: (res) => {
                    this.setState({
                        logs: res.logs,
                        pagination: res.pagination,
                        is_loading_logs: false
                    }, () => {
                        window.KTMenu.createInstances();
                    })
                },
                error: (res) => {
                    this.setState({
                        is_loading: false,
                    })
                }
            }
        )
    }


    render() {
        let user = this.state.user;

        let statusClass = 'badge badge-light-info';
        if (user.user_status === 'active') {
            statusClass = 'badge badge-light-success';
        }
        if (user.user_status === 'ban') {
            statusClass = 'badge badge-lght-danger';
        }
        return (
            <>
                <div className="toolbar" id="kt_toolbar">
                    <div id="kt_toolbar_container" className="container-fluid d-flex flex-stack">
                        <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                            data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                            className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                            <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">User</h1>
                            <span className="h-20px border-gray-200 border-start mx-4"></span>
                            <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                                <li className="breadcrumb-item text-muted">
                                    <Link to="/" className="text-muted text-hover-primary">Dashboard</Link>
                                </li>
                                <li className="breadcrumb-item">
                                    <span className="bullet bg-gray-200 w-5px h-2px"></span>
                                </li>
                                <li className="breadcrumb-item text-dark">
                                    <Link to={'/user'} className="text-muted text-hover-primary">List</Link>
                                </li>
                                <li className="breadcrumb-item">
                                    <span className="bullet bg-gray-200 w-5px h-2px"></span>
                                </li>
                                <li className="breadcrumb-item text-dark">View</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" className="container-fluid">

                        <div className='row g-xxl-9'>

                            <div className='col-xxl-3'>
                                <div className="card mb-6 mb-xl-2">
                                    <div className='card-body pt-9'>
                                        {
                                            this.state.is_loading
                                                && !this.state.user ?
                                                <div className="d-flex" style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                                                    <span className="spinner-border spinner-border-md align-middle"></span> <p>Loading</p>
                                                </div>
                                                : null
                                        }
                                        {
                                            this.state.is_loading
                                                && this.state.user ?
                                                <div className="absolute_loading" >
                                                    <div className="d-flex" style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                                                        <span className="spinner-border spinner-border-md align-middle"></span> <p>Loading</p>
                                                    </div>
                                                </div>
                                                : null
                                        }
                                        {

                                            !this.state.is_loading
                                                && this.state.user ?
                                                <>

                                                    <div className="d-flex align-items-center mb-1" >
                                                        <div className="text-gray-800 fs-2 fw-bolder me-3">{user.user_name}</div>
                                                        <span className={statusClass} style={{ textTransform: "capitalize" }}>{user.user_status}</span>
                                                    </div>
                                                    <div className='separator separator-dashed my-3' />
                                                    <div>
                                                        <div className="fw-bolder mt-5">
                                                            <span className="svg-icon svg-icon-4 me-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM12 7C10.3 7 9 8.3 9 10C9 11.7 10.3 13 12 13C13.7 13 15 11.7 15 10C15 8.3 13.7 7 12 7Z" fill="black"></path>
                                                                    <path d="M12 22C14.6 22 17 21 18.7 19.4C17.9 16.9 15.2 15 12 15C8.8 15 6.09999 16.9 5.29999 19.4C6.99999 21 9.4 22 12 22Z" fill="black"></path>
                                                                </svg>
                                                            </span>
                                                            Guest ID
                                                        </div>
                                                        <div className="text-gray-600">{user.guest_id}</div>
                                                        <div className="fw-bolder mt-5">
                                                            <span className="svg-icon svg-icon-4 me-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z" fill="black" />
                                                                    <path opacity="0.3" d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            User ID
                                                        </div>
                                                        <div className="text-gray-600">{user.user_id}</div>
                                                        <div className="fw-bolder mt-5">
                                                            <span className="svg-icon svg-icon-4 me-1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black" />
                                                                    <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black" />
                                                                    <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            Created
                                                        </div>
                                                        <div className="text-gray-600">{Helper.getDate(user.create_date, { is_show_time: true })}</div>
                                                    </div>

                                                </>
                                                : null
                                        }

                                    </div>
                                </div>
                            </div>

                            <div className='col-xxl-9'>
                                {/* <div className='d-flex'>
                                    <div className='d-flex mb-4' style={{ marginLeft: 'auto' }}>
                                        <div className="me-0">
                                            <button className="btn btn-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                                Actions
                                            </button>
                                            <div className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
                                                <div className="menu-item px-3">
                                                    <div className="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Actions</div>
                                                </div>
                                                <div className="menu-item px-3">
                                                    <a href="#" className="menu-link px-3">Create Invoice</a>
                                                </div>
                                                <div className="menu-item px-3">
                                                    <a href="#" className="menu-link flex-stack px-3">Create Payment
                                                        <i className="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Specify a target name for future usage and reference" aria-label="Specify a target name for future usage and reference"></i></a>
                                                </div>
                                                <div className="menu-item px-3">
                                                    <a href="#" className="menu-link px-3">Generate Bill</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                                {
                                    !this.state.is_loading
                                        && this.state.user ?
                                        <div className='row g-xxl-9'>
                                            <div className='col-xxl-5'>
                                                <div className='card '>
                                                    <div className="card-header card-header-stretch">
                                                        <div className="card-title">
                                                            <h3 className="m-0 text-gray-900">Played</h3>
                                                        </div>
                                                    </div>
                                                    <div className='card-body pt-7 pb-0 px-0 '>
                                                        <div className="row p-0 mb-5 px-9">
                                                            <div className="col">
                                                                <div className="border border-dashed border-gray-300 text-center min-w-100px rounded pt-4 pb-2 my-3">
                                                                    <span className="fs-4 fw-bold text-gray-400 d-block">Dummy</span>
                                                                    <span className="fs-2hx fw-bolder text-gray-900 counted">{Helper.numberFormat(user.played_dummy)}</span>
                                                                </div>
                                                            </div>
                                                            <div className="col">
                                                                <div className="border border-dashed border-gray-300 text-center min-w-100px rounded pt-4 pb-2 my-3">
                                                                    <span className="fs-4 fw-bold text-gray-400 d-block">Pokdeng</span>
                                                                    <span className="fs-2hx fw-bolder text-gray-900 counted">{Helper.numberFormat(user.played_pokdeng)}</span>
                                                                </div>
                                                            </div>
                                                            <div className="col">
                                                                <div className="border border-dashed border-gray-300 text-center min-w-100px rounded pt-4 pb-2 my-3">
                                                                    <span className="fs-4 fw-bold text-gray-400 d-block">Slave</span>
                                                                    <span className="fs-2hx fw-bolder text-gray-900 counted">{Helper.numberFormat(user.played_slave)}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className='col-xxl-7'>
                                                <div className='card'>
                                                    <div className="card-header card-header-stretch">
                                                        <div className="card-title">
                                                            <h3 className="m-0 text-gray-900">Wallet</h3>
                                                        </div>
                                                    </div>
                                                    <div className='card-body pt-7 pb-0 px-0'>
                                                        <div className="row p-0 mb-5 px-9">
                                                            <div className="col">
                                                                <div className="border border-dashed border-gray-300 text-center min-w-125px rounded pt-4 pb-2 my-3">
                                                                    <span className="fs-4 fw-bold text-gray-400 d-block">Chip</span>
                                                                    <span className="fs-2hx fw-bolder text-gray-900 counted">{Helper.numberFormat(user.user_chip)}</span>
                                                                </div>
                                                            </div>
                                                            <div className="col">
                                                                <div className="border border-dashed border-gray-300 text-center min-w-125px rounded pt-4 pb-2 my-3">
                                                                    <span className="fs-4 fw-bold text-gray-400 d-block">Coin</span>
                                                                    <span className="fs-2hx fw-bolder text-gray-900 counted">{Helper.numberFormat(user.user_coin)}</span>
                                                                </div>
                                                            </div>

                                                            <div className="col">
                                                                <div className="border border-dashed border-gray-300 text-center min-w-125px rounded pt-4 pb-2 my-3">
                                                                    <span className="fs-4 fw-bold text-gray-400 d-block">Highest Chip</span>
                                                                    <span className="fs-2hx fw-bolder text-gray-900 counted">{Helper.numberFormat(user.total_highest_chip)}</span>
                                                                </div>
                                                            </div>
                                                            <div className="col">
                                                                <div className="border border-dashed border-gray-300 text-center min-w-125px rounded pt-4 pb-2 my-3">
                                                                    <span className="fs-4 fw-bold text-gray-400 d-block">Highest Coin</span>
                                                                    <span className="fs-2hx fw-bolder text-gray-900 counted">{Helper.numberFormat(user.total_highest_coin)}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div className='col-xxl-12'>
                                                <div class="card ">
                                                    <div class="card-header card-header-stretch">
                                                        <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0">
                                                            {/* <li class="nav-item">
                                                                <a class="nav-link active" data-bs-toggle="tab" href="#payment_history">Payment History</a>
                                                            </li> */}
                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-bs-toggle="tab" href="#logs">Logs</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="tab-content" id="myTabContent">
                                                            {/* <div class="tab-pane fade show active" id="payment_history" role="tabpanel">
                                                                No Data
                                                            </div> */}

                                                            <div class="tab-pane fade show active" id="logs" role="tabpanel">
                                                                <div className="table-responsive">
                                                                    <table className="table align-middle table-row-dashed gs-0 gy-4 no-footer" id="kt_customers_table" role="grid">
                                                                        <thead>
                                                                            <tr className="fw-bolder text-muted bg-light">
                                                                                <th className="min-w-150px ps-4 rounded-start">Type</th>
                                                                                <th className="min-w-150px">Amount</th>
                                                                                <th className="min-w-150px">Event</th>
                                                                                <th className="min-w-100px pe-4 rounded-end">Created</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody className="fw-bold text-gray-600" style={{ position: 'relative' }}>
                                                                            {
                                                                                this.state.logs.length > 0 ?
                                                                                    this.state.logs.map((log, log_i) => {
                                                                                        let type = "-";
                                                                                        if (log.type) {
                                                                                            type = log.type.replace("_", " ");
                                                                                        }
                                                                                        return (
                                                                                            <tr key={log_i}>
                                                                                                <td className='ps-4' style={{ textTransform: 'capitalize' }}>
                                                                                                    {type}
                                                                                                </td>
                                                                                                <td>
                                                                                                    {Helper.numberFormat(log.amount)}
                                                                                                </td>
                                                                                                <td style={{ textTransform: 'capitalize' }}>
                                                                                                    {log.event}
                                                                                                </td>
                                                                                                <td className="pe-3">
                                                                                                    {Helper.getDate(log.created, { is_show_time: true })}
                                                                                                </td>
                                                                                            </tr>
                                                                                        )
                                                                                    })
                                                                                    :
                                                                                    !this.state.is_loading ?
                                                                                        <tr>
                                                                                            <td colSpan="7" align="center">No Data</td>
                                                                                        </tr>
                                                                                        : null
                                                                            }
                                                                            {
                                                                                this.state.is_loading
                                                                                    && this.state.logs.length === 0 ?
                                                                                    <tr>
                                                                                        <td colSpan="7" align="center"><span className="spinner-border spinner-border-md align-middle"></span></td>
                                                                                    </tr>
                                                                                    : null
                                                                            }
                                                                            {
                                                                                this.state.is_loading
                                                                                    && this.state.logs.length > 0 ?
                                                                                    <div className="absolute_loading" >
                                                                                        <span className="spinner-border spinner-border-md align-middle"></span>
                                                                                    </div>
                                                                                    : null
                                                                            }
                                                                        </tbody>
                                                                    </table>

                                                                </div>

                                                                {
                                                                    this.state.logs.length > 0
                                                                        && this.state.pagination ?
                                                                        <Pagination
                                                                            pagination={this.state.pagination}
                                                                            onChangePage={(page) => {
                                                                                let qs = this.state.qs;
                                                                                qs.page = page;
                                                                                let url = Helper.getQueryStringLinkFromObject(qs);
                                                                                navigate(url);
                                                                            }}
                                                                        />
                                                                        : null
                                                                }
                                                                {
                                                                    this.state.is_loading_logs
                                                                        && this.state.logs.length === 0 ?
                                                                        <tr>
                                                                            <td colSpan="7" align="center"><span className="spinner-border spinner-border-md align-middle"></span></td>
                                                                        </tr>
                                                                        : null
                                                                }
                                                                {
                                                                    this.state.is_loading_logs
                                                                        && this.state.logs.length > 0 ?
                                                                        <div className="absolute_loading" >
                                                                            <span className="spinner-border spinner-border-md align-middle"></span>
                                                                        </div>
                                                                        : null
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        : null
                                }
                            </div>
                        </div>
                    </div >
                </div >

            </>
        );
    }
}