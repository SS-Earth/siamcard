import React from 'react';
import { Link, navigate } from '@reach/router';
import Pagination from './../Component/Pagination';
import Helper from '../Helper';
import API from './../API';
const limit = 10;
export default class Logs extends React.Component {
    constructor() {
        super();
        this.state = {
            is_loading: true,
            logs: [],
            pagination: false,
            qs: {
                page: Helper.getParameterByName('page') ? Helper.getParameterByName('page') : 1,
                limit: Helper.getParameterByName('limit') ? parseFloat(Helper.getParameterByName('limit')) : limit,
                keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
            },
        }
    }

    componentDidMount() {
        this.setState({
            is_loading: false,
        })
    }

    componentDidMount() {
        this.fetch();
    }
    componentWillUnmount() {
        API.abort();
    }
    componentDidUpdate(prevProps) {
        if (this.props.location.search !== prevProps.location.search) {
            this.setState({
                is_loading: true,
                // users: [],
                // pagination: false,
                qs: {
                    page: Helper.getParameterByName('page') ? Helper.getParameterByName('page') : 1,
                    limit: Helper.getParameterByName('limit') ? parseFloat(Helper.getParameterByName('limit')) : limit,
                    keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
                },
            }, () => {
                this.fetch();
            })
        }
        if (this.props.location.search !== prevProps.location.search && !this.props.location.search) {
            this.setState({
                is_loading: true,
                // users: [],
                // pagination: false,
                qs: {
                    page: 1,
                    limit: 8,
                    keyword: Helper.getParameterByName('keyword') ? Helper.getParameterByName('keyword') : "",
                },
            }, () => {
                this.fetch();
            })
        }
    }

    fetch() {
        let filter = { ...this.state.qs };
        this.call = API.get(`/logs`,
            filter,
            {
                success: (res) => {
                    this.setState({
                        logs: res.logs,
                        pagination: res.pagination,
                        is_loading: false
                    }, () => {
                        window.KTMenu.createInstances();
                    })
                },
                error: (res) => {
                    this.setState({
                        is_loading: false,
                    })
                }
            }
        )
    }

    render() {
        return (
            <>
                <div className="toolbar" id="kt_toolbar">
                    <div id="kt_toolbar_container" className="container-fluid d-flex flex-stack">
                        <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                            data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                            className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                            <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Logs</h1>
                            <span className="h-20px border-gray-200 border-start mx-4"></span>
                            <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                                <li className="breadcrumb-item text-muted">
                                    <Link to="/" className="text-muted text-hover-primary">Dashboard</Link>
                                </li>
                                <li className="breadcrumb-item">
                                    <span className="bullet bg-gray-200 w-5px h-2px"></span>
                                </li>
                                <li className="breadcrumb-item text-dark">List</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="post d-flex flex-column-fluid" id="kt_post">
                    <div id="kt_content_container" className="container-fluid">
                        <div className="card">
                            <div className="card-header border-0 pt-6">
                                <div className="card-title">
                                    <form className="d-flex align-items-center position-relative my-1"
                                        ref={(ref => { this.$search = window.$(ref) })}
                                        onSubmit={(e) => {
                                            e.preventDefault();
                                            let data = Helper.getFormInputObject(this.$search);
                                            // console.log(data)
                                            let url = Helper.getQueryStringLinkFromObject(data)
                                            navigate(url);
                                        }}
                                    >
                                        <span className="svg-icon svg-icon-1 position-absolute ms-6">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black"></rect>
                                                <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black"></path>
                                            </svg>
                                        </span>
                                        <input
                                            type="text"
                                            data-kt-customer-table-filter="search"
                                            className="form-control form-control-solid w-250px ps-15"
                                            placeholder="Search By User ID"
                                            name="keyword"
                                            defaultValue={this.state.qs.keyword}
                                        />
                                        <button type="submit" style={{ display: 'none' }}>Search</button>
                                    </form>
                                </div>
                            </div>
                            <div className="card-body">
                                <div id="kt_customers_table_wrapper" className="dataTables_wrapper dt-bootstrap4 no-footer">
                                    <div className="table-responsive">
                                        <table className="table align-middle table-row-dashed gs-0 gy-4 no-footer" id="kt_customers_table" role="grid">
                                            <thead>
                                                <tr className="fw-bolder text-muted bg-light">
                                                    <th className="min-w-150px ps-4 rounded-start" style={{ verticalAlign: 'middle' }}>User</th>
                                                    <th className="min-w-150px">Type</th>
                                                    <th className="min-w-150px">Amount</th>
                                                    <th className="min-w-150px">Event</th>
                                                    <th className="min-w-100px pe-4 rounded-end">Created</th>
                                                </tr>
                                            </thead>
                                            <tbody className="fw-bold text-gray-600" style={{ position: 'relative' }}>
                                                {
                                                    this.state.logs.length > 0 ?
                                                        this.state.logs.map((log, log_i) => {
                                                            let type = "-";
                                                            if(log.type) {
                                                                type = log.type.replace("_"," ");
                                                            }
                                                            return (
                                                                <tr key={log_i}>
                                                                    <td className="ps-4 ">

                                                                        <div className="d-flex flex-column">
                                                                            <div>
                                                                                <Link to={`/user/${log.user.user_id}`} className="text-dark text-hover-primary fs-6 fw-bolder">{log.user.user_name}</Link>
                                                                            </div>
                                                                            <span className="text-muted fw-bold">User ID : {log.user.user_id}</span>
                                                                        </div>
                                                                    </td>
                                                                    <td style={{textTransform: 'capitalize'}}>
                                                                        {type}
                                                                    </td>
                                                                    <td>
                                                                        {Helper.numberFormat(log.amount)}
                                                                    </td>
                                                                    <td style={{textTransform: 'capitalize'}}>
                                                                        {log.event}
                                                                    </td>
                                                                    <td className="pe-3">
                                                                        {Helper.getDate(log.created, { is_show_time: true })}
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })
                                                        :
                                                        !this.state.is_loading ?
                                                            <tr>
                                                                <td colSpan="7" align="center">No Data</td>
                                                            </tr>
                                                            : null
                                                }
                                                {
                                                    this.state.is_loading
                                                        && this.state.logs.length === 0 ?
                                                        <tr>
                                                            <td colSpan="7" align="center"><span className="spinner-border spinner-border-md align-middle"></span></td>
                                                        </tr>
                                                        : null
                                                }
                                                {
                                                    this.state.is_loading
                                                        && this.state.logs.length > 0 ?
                                                        <div className="absolute_loading" >
                                                            <span className="spinner-border spinner-border-md align-middle"></span>
                                                        </div>
                                                        : null
                                                }
                                            </tbody>
                                        </table>

                                    </div>

                                    {
                                        this.state.logs.length > 0
                                            && this.state.pagination ?
                                            <Pagination
                                                pagination={this.state.pagination}
                                                onChangePage={(page) => {
                                                    let qs = this.state.qs;
                                                    qs.page = page;
                                                    let url = Helper.getQueryStringLinkFromObject(qs);
                                                    navigate(url);
                                                }}
                                            />
                                            : null
                                    }


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}