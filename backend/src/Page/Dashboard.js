import React from 'react';
import Swal from 'sweetalert2';
import API from './../API';

export default class Dashboard extends React.Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      id: 16900,
    }
  }


  async callAPI() {
    let res = await API.post(`/user/coin/increase`, {
      amount: 150, 
      user_id: this.state.id,
      event: 'pokdeng'
    });
    console.log(res)
    Swal.fire({
      title: 'Success!',
      text: 'Change State Success',
      icon: 'success',
      confirmButtonText: 'OK!'
    })
  }

  render() {
    return (
      <>
        <div className="toolbar" id="kt_toolbar">
          <div id="kt_toolbar_container" className="container-fluid d-flex flex-stack">
            <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
              data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
              className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
              <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Dashboard
                <span className="h-20px border-gray-200 border-start ms-3 mx-2"></span>
                <small className="text-muted fs-7 fw-bold my-1 ms-1">#Adminitrator</small>
              </h1>
            </div>
          </div>
        </div>

        <div className="post d-flex flex-column-fluid" id="kt_post">
          <div id="kt_content_container" className="container-fluid">
            {/* <div className={"mb-3"}>Dashboard Content {this.state.count}</div>
            <button
              className={"btn btn-success me-3"}
              onClick={() => {
                let plus = this.state.count + 1;
                this.setState({
                  count: plus
                }, async () => {
                  let res = await API.post(`/event/daily`, {user_id: 16974});
                  console.log(res)
                  Swal.fire({
                    title: 'Success!',
                    text: 'Change State Success',
                    icon: 'success',
                    confirmButtonText: 'OK!'
                  })
                })

              }}>Test Alert</button>
            <button
              className={"btn btn-primary me-3"}
              onClick={() => {
                let plus = this.state.count + 1;
                this.setState({
                  count: plus
                }, async () => {
                  let res = await API.get(`/account`);
                  Swal.fire({
                    title: 'Success!',
                    text: 'Change State Success',
                    icon: 'success',
                    confirmButtonText: 'OK!'
                  })
                })

              }}>Test Get Account</button>
              <button className='btn btn-info' 
                onClick={()=> {
                  this.callAPI();
                }}
              >callAPI</button> */}
          </div>
        </div>
      </>
    );
  }
}