import React from 'react';
import Helper from './../Helper';
import Swal from 'sweetalert2';
import HmacSHA256 from 'crypto-js/hmac-sha256';
import Hex from 'crypto-js/enc-hex';

export default class Register extends React.Component {

    onSubmit(data) {
        let pwd = Hex.stringify(HmacSHA256(data.password, window.secretkey));
        data.password = pwd;
        window.$.ajax({
            method: 'POST',
            url: `${window.api_host}/create-admin`,
            data: { ...data },
            beforeSend: () => {
                Helper.setButtonLoading(window.$("#kt_sign_in_form").find('[type="submit"]'))
            },
            success: (res) => {
                if (res.is_success) {
                    Swal.fire({
                        title: 'Success!',
                        text: 'Register Successful',
                        icon: 'success',
                        confirmButtonText: 'OK!'
                    })
                    window.location.href= window.location.href
                }
                if (!res.is_success) {
                    Swal.fire({
                        // title: 'Warning!',
                        text: res.message,
                        icon: 'warning',
                        confirmButtonText: 'OK!'
                    })
                }
            },
            error: () => {
                Swal.fire({
                    title: 'Error!',
                    text: 'No Login function',
                    icon: 'error',
                    confirmButtonText: 'OK!'
                })
            },
            complete: () => {
                Helper.setButtonLoaded(window.$("#kt_sign_in_form").find('[type="submit"]'));
            }
        })
    }



    render() {
        return (
            <>
                <div className="d-flex flex-column flex-root">
                    <div className="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style={{ backgroundImage: `url('assets/media/illustrations/development-hd.png')` }}>
                        <div className="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                            <a href="#" className="mb-12">
                                <img alt="Logo" src="./assets/media/logos/logo-2-dark.svg" className="h-45px" />
                            </a>
                            <div className="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                                <form className="form w-100" noValidate="noValidate" id="kt_sign_in_form"
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                        let data = Helper.getFormInputObject(window.$(e.target))
                                        // console.log(data);
                                        this.onSubmit(data);
                                    }}
                                >
                                    <div className="text-center mb-10">
                                        <h1 className="text-dark mb-3">Sign In to SiamCard</h1>
                                    </div>
                                    <div className="fv-row mb-10">
                                        <label className="form-label fs-6 fw-bolder text-dark">Email</label>
                                        <input className="form-control form-control-lg form-control-solid" type="text" name="email" autoComplete="off" />
                                    </div>
                                    <div className="fv-row mb-10">
                                        <div className="d-flex flex-stack mb-2">
                                            <label className="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                                            {/* <a href="../../demo1/dist/authentication/flows/basic/password-reset.html" className="link-primary fs-6 fw-bolder">Forgot Password ?</a> */}
                                        </div>
                                        <input className="form-control form-control-lg form-control-solid" type="password" name="password" autoComplete="off" />
                                    </div>
                                    <div className="fv-row mb-10">
                                        <label className="form-label fs-6 fw-bolder text-dark">Name</label>
                                        <input className="form-control form-control-lg form-control-solid" type="text" name="name" autoComplete="off" />
                                    </div>
                                    <div className="text-center">
                                        <button type="submit" id="kt_sign_in_submit" className="btn btn-lg btn-primary w-100 mb-5">
                                            <span className="indicator-label">Continue</span>
                                            <span className="indicator-progress">Please wait...
                                                <span className="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                        </button>
                                        {/* <div className="text-center text-muted text-uppercase fw-bolder mb-5">or</div>
                                        <a href="#" className="btn btn-flex flex-center btn-light btn-lg w-100 mb-5">
                                            <img alt="Logo" src="assets/media/svg/brand-logos/google-icon.svg" className="h-20px me-3" />Continue with Google</a>
                                        <a href="#" className="btn btn-flex flex-center btn-light btn-lg w-100 mb-5">
                                            <img alt="Logo" src="assets/media/svg/brand-logos/facebook-4.svg" className="h-20px me-3" />Continue with Facebook</a>
                                        <a href="#" className="btn btn-flex flex-center btn-light btn-lg w-100">
                                            <img alt="Logo" src="assets/media/svg/brand-logos/apple-black.svg" className="h-20px me-3" />Continue with Apple</a> */}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}