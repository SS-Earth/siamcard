import React from 'react';
import Helper from '../Helper';


export default class Pagination extends React.Component {
    render() {
        let pages = [];
        for (let index = 0; index < this.props.pagination.pages; index++) {
            pages.push(index + 1);
        }
        let offset = (parseFloat(this.props.pagination.limit) * (parseFloat(this.props.pagination.now_page) - 1)) + 1;
        let to = parseFloat(this.props.pagination.limit) * parseFloat(this.props.pagination.now_page);
        if (to > this.props.pagination.count_all_items) {
            to = this.props.pagination.count_all_items
        }
        return (
            <div className="d-flex flex-wrap align-items-center">
                <div>
                    <div className="dataTables_paginate paging_simple_numbers" id="kt_customers_table_paginate">
                        <ul className="pagination">
                            <li className={"paginate_button page-item previous" + (this.props.pagination.now_page === 1 ? " disabled" : "")} id="kt_customers_table_previous">
                                <div
                                    onClick={() => {
                                        if (this.props.pagination.now_page === 1) { return; }
                                        this.props.onChangePage((parseFloat(this.props.pagination.now_page) - 1))
                                    }}
                                    style={{ cursor: 'pointer' }}
                                    aria-controls="kt_customers_table"
                                    className="page-link">
                                    <i className="previous"></i>
                                </div>
                            </li>
                            {
                                pages.map((page, page_i) => {
                                    let responseClass = "paginate_button page-item ";
                                    if (page === this.props.pagination.now_page) {
                                        responseClass = responseClass + ' disabled active'
                                    }
                                    return (<li key={page_i} className={responseClass} >
                                        <div
                                            onClick={() => {
                                                this.props.onChangePage(page)
                                            }}
                                            style={{ cursor: 'pointer' }}
                                            aria-controls="kt_customers_table"
                                            className="page-link">
                                            {page}
                                        </div>
                                    </li>
                                    )
                                })
                            }

                            <li className={"paginate_button page-item next" + (this.props.pagination.now_page === this.props.pagination.pages ? " disabled" : "")} id="kt_customers_table_next">
                                <div
                                    onClick={() => {
                                        if (this.props.pagination.now_page === this.props.pagination.pages) { return; }
                                        this.props.onChangePage((parseFloat(this.props.pagination.now_page) + 1))
                                    }}
                                    style={{ cursor: 'pointer' }}
                                    aria-controls="kt_customers_table"
                                    className="page-link">
                                    <i className="next"></i>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div style={{ marginLeft: 'auto' }}>
                    Showing {offset} to {to} of {Helper.numberFormat(this.props.pagination.count_all_items)} entries
                </div>
            </div>

        )
    }
}