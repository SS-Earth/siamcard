import React from 'react';
import Swal from 'sweetalert2';
import API from '../API';
import Helper from '../Helper';
import HmacSHA256 from 'crypto-js/hmac-sha256';
import Hex from 'crypto-js/enc-hex';

export default class Index extends React.Component {
    render() {
        return <ModalForm />
    }
}

class ModalForm extends React.Component {
    async changePassword(data) {
        let old_pwd_encypt = Hex.stringify(HmacSHA256(data.old_password, window.secretkey));
        let new_pwd_encypt = Hex.stringify(HmacSHA256(data.new_password, window.secretkey));
        data.old_pwd_encypt = old_pwd_encypt;
        data.new_pwd_encypt = new_pwd_encypt;
        API.put('/admin-change-password', data, {
            beforeSend: () => {
                Helper.setButtonLoading(this.$submit);
            },
            success: (res) => {
                if (res.is_success) {
                    Swal.fire({
                        title: 'Success!',
                        text: 'Register Successful',
                        icon: 'success',
                        confirmButtonText: 'OK!',
                        timer: 3000,
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-light'
                        },
                        buttonsStyling: false,
                        reverseButtons: true
                    })
                    this.$modal.modal('hide')
                }
                if (!res.is_success) {
                    Swal.fire({
                        // title: 'Warning!',
                        text: res.message,
                        icon: 'warning',
                        confirmButtonText: 'OK!',
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-light'
                        },
                        buttonsStyling: false,
                        reverseButtons: true
                    })
                    Helper.setButtonLoaded(this.$submit)
                }
            },
            error: (res) => {
                Swal.fire({
                    title: 'Error!',
                    text: 'No Login function',
                    icon: 'error',
                    confirmButtonText: 'OK!',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-light'
                    },
                    buttonsStyling: false,
                    reverseButtons: true
                })
                Helper.setButtonLoaded(this.$submit)
            }
        });
    }
    render() {
        return (
            <div ref={(ref) => {
                this.$modal = window.$(ref);
                this.$modal.modal('show');
                this.$modal.on('hidden.bs.modal', () => {
                    if (this.props.onCancel) {
                        this.props.onCancel();
                    }
                    let settime = window.setTimeout(() => {
                        this.$modal.remove();
                        clearTimeout(settime);
                    }, 500)
                });
                this.$form = this.$modal.find('form');
                this.$submit = this.$form.find('[type="submit"]')
            }}
                className="modal fade" tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <form className="modal-content"
                        ref={(ref) => {
                            window.$(ref).off().on('submit', (e) => {
                                e.preventDefault();
                                let data = Helper.getFormInputObject(this.$form);
                                this.changePassword(data)
                            })
                        }}
                    >
                        <div className="modal-header">
                            <h4 className="modal-title">Change Password</h4>
                        </div>
                        <div className="modal-body">
                            <div className="fv-row mb-10">
                                <div className="d-flex flex-stack mb-2">
                                    <label className="form-label fw-bolder text-dark fs-6 mb-0">Old Password</label>
                                    {/* <a href="../../demo1/dist/authentication/flows/basic/password-reset.html" className="link-primary fs-6 fw-bolder">Forgot Password ?</a> */}
                                </div>
                                <input className="form-control form-control-lg form-control-solid" type="password" name="old_password" autoComplete="off" />
                            </div>
                            <div className="fv-row mb-10">
                                <div className="d-flex flex-stack mb-2">
                                    <label className="form-label fw-bolder text-dark fs-6 mb-0">New Password</label>
                                    {/* <a href="../../demo1/dist/authentication/flows/basic/password-reset.html" className="link-primary fs-6 fw-bolder">Forgot Password ?</a> */}
                                </div>
                                <input className="form-control form-control-lg form-control-solid" type="password" name="new_password" autoComplete="off" />
                            </div>
                            <div className="fv-row mb-10">
                                <div className="d-flex flex-stack mb-2">
                                    <label className="form-label fw-bolder text-dark fs-6 mb-0">Confirm New Password</label>
                                    {/* <a href="../../demo1/dist/authentication/flows/basic/password-reset.html" className="link-primary fs-6 fw-bolder">Forgot Password ?</a> */}
                                </div>
                                <input className="form-control form-control-lg form-control-solid" type="password" name="confirm_password" autoComplete="off" />
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="reset" className="btn btn-light" data-bs-dismiss="modal"
                            >Cancel</button>
                            <button type="submit" className="btn btn-primary ">
                                <span className="indicator-label">Change Password</span>
                                <span className="indicator-progress">Please wait...
                                    <span className="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}