import React from 'react';
import { Link } from "@reach/router";

export default class AsideMenu extends React.Component {
	render() {
		return (
			<div id="kt_aside" className="aside aside-dark aside-hoverable" data-kt-drawer="true"
				data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
				data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}"
				data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
				<div className="aside-logo flex-column-auto" id="kt_aside_logo">
					<Link to="/">
						<img alt="Logo" src="./assets/media/logos/logo-1.svg" className="h-15px logo" />
					</Link>
					<div id="kt_aside_toggle" className="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle"
						data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
						data-kt-toggle-name="aside-minimize"
						ref={(ref) => {
							// let _this = window.$(ref);
							// _this.off().on('click',()=> {
							//     let defaultToggle = window.$('#kt_body').attr("data-kt-aside-minimize");
							//     console.log(defaultToggle)
							//     if(!defaultToggle ) {
							//         window.$('#kt_body').attr("data-kt-aside-minimize", 'on');
							//         _this.addClass('active');
							//         window.$('#kt_aside').addClass('animating');
							//     } else {
							//         window.$('#kt_body').removeAttr("data-kt-aside-minimize");
							//         _this.removeClass('active');
							//         window.$('#kt_aside').addClass('animating');
							//     }
							//     setTimeout(()=> {
							//         window.$('#kt_aside').removeClass('animating');
							//     },300)
							// })
						}}
					>
						<span className="svg-icon svg-icon-1 rotate-180">
							<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
								width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								<g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
									<polygon points="0 0 24 0 24 24 0 24" />
									<path
										d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
										fill="#000000" fillRule="nonzero"
										transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
									<path
										d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
										fill="#000000" fillRule="nonzero" opacity="0.5"
										transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
								</g>
							</svg>
						</span>
					</div>
				</div>
				<div className="aside-menu flex-column-fluid">
					<div className="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true"
						data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto"
						data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
						data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">

						<div className="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500"
							id="#kt_aside_menu" data-kt-menu="true">
							<div className="menu-item">
								<div className="menu-content pb-2">
									<span className="menu-section text-muted text-uppercase fs-8 ls-1">Menu</span>
								</div>
							</div>
							<div className="menu-item">
								<Link className="menu-link" to="/" active-path="/dashboard"
									getProps={({ isCurrent }) => {
										return {
											className: isCurrent || window.location.pathname === "/" || window.location.pathname === "" ? "menu-link active" : "menu-link"
										};
									}}
								>
									<span className="menu-icon">
										<i className="bi bi-house fs-3"></i>
									</span>
									<span className="menu-title">Dashboard</span>
								</Link>
								<Link className="menu-link" to="/user" active-path="/user"
									getProps={({ isCurrent }) => {
										return {
											className: isCurrent || window.location.pathname.indexOf("/user") !== -1 || window.location.pathname === "" ? "menu-link active" : "menu-link"
										};
									}}
								>
									<span className="menu-icon">
										<i className="las la-user fs-3"></i>
									</span>
									<span className="menu-title">User</span>
								</Link>
								<Link className="menu-link" to="/logs" active-path="/logs"
									getProps={({ isCurrent }) => {
										return {
											className: isCurrent || window.location.pathname.indexOf("/logs") !== -1 || window.location.pathname === "" ? "menu-link active" : "menu-link"
										};
									}}
								>
									<span className="menu-icon">
										<i class="las la-clipboard-list fs-3"></i>
									</span>
									<span className="menu-title">Logs</span>
								</Link>
								{
									this.props.admin && this.props.admin.is_admin ?
										<Link className="menu-link" to="/account" active-path="/account"
											getProps={({ isCurrent }) => {
												return {
													className: isCurrent || window.location.pathname.indexOf("/account") !== -1 || window.location.pathname === "" ? "menu-link active" : "menu-link"
												};
											}}
										>
											<span className="menu-icon">
												<i className="las la-user-cog fs-3"></i>
											</span>
											<span className="menu-title">Account</span>
										</Link>
										: null
								}
								{/* <Link className="menu-link" to="/test-app">
									<span className="menu-icon">
										<i className="bi bi-house fs-3"></i>
									</span>
									<span className="menu-title">TestApp</span>
								</Link> */}
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}