export const pageFunction = {
    init: ()=> {
        var dropdownElementList = [].slice.call(document.querySelectorAll('.dropdown-toggle'))
        var dropdownList = dropdownElementList.map(function (dropdownToggleEl) {
        return new window.bootstrap.Dropdown(dropdownToggleEl)
        })
    }
}