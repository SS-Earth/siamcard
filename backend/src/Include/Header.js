import React from 'react';
import Helper from './../Helper';
import ChangePassword from './../Component/ChangePassword';
import { Link } from '@reach/router';

export default class Header extends React.Component {
    componentDidMount() {
        window.KTMenu.createInstances();
    }

    logout() {
        Helper.setCookie('login_token', '', 1)
        window.location.href = '/';
    }

    render() {
        return (
            <div id="kt_header" className="header align-items-stretch">
                <div className="container-fluid d-flex align-items-stretch justify-content-between">
                    <div className="d-flex align-items-center d-lg-none ms-n3 me-1" title="Show aside menu">
                        <div className="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px"
                            id="kt_aside_mobile_toggle">
                            <span className="svg-icon svg-icon-2x mt-1">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
                                    width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <rect fill="#000000" x="4" y="5" width="16" height="3" rx="1.5" />
                                        <path
                                            d="M5.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L5.5,18 C4.67157288,18 4,17.3284271 4,16.5 C4,15.6715729 4.67157288,15 5.5,15 Z M5.5,10 L18.5,10 C19.3284271,10 20,10.6715729 20,11.5 C20,12.3284271 19.3284271,13 18.5,13 L5.5,13 C4.67157288,13 4,12.3284271 4,11.5 C4,10.6715729 4.67157288,10 5.5,10 Z"
                                            fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div className="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
                        <a href="" className="d-lg-none">
                            <img alt="Logo" src="assets/media/logos/logo-3.svg" className="h-30px" />
                        </a>
                    </div>
                    <div className="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
                        <div className="d-flex align-items-stretch" id="kt_header_nav">
                            <div className="header-menu align-items-stretch" data-kt-drawer="true"
                                data-kt-drawer-name="header-menu"
                                data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true"
                                data-kt-drawer-width="{default:'200px', '300px': '250px'}"
                                data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_header_menu_mobile_toggle"
                                data-kt-swapper="true" data-kt-swapper-mode="prepend"
                                data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
                                <div className="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch"
                                    id="#kt_header_menu" data-kt-menu="true">
                                    {/* <div className="menu-item me-lg-1">
                                        <a className="menu-link active py-3" href="../../demo1/dist/index.html">
                                            <span className="menu-title">Dashboard</span>
                                        </a>
                                    </div> */}
                                </div>
                            </div>
                        </div>

                        <div className="d-flex align-items-stretch flex-shrink-0">
                            <div className="d-flex align-items-stretch flex-shrink-0">
                                <div className="d-flex align-items-center ms-1 ms-lg-3 " id="kt_header_user_menu_toggle">
                                    <div ref={(ref) => {
                                        let _this = window.$(ref);
                                        _this.off().on('click', () => {
                                            _this.addClass('show')
                                        })
                                    }}
                                        className="cursor-pointer symbol symbol-30px symbol-md-40px menu-dropdown"
                                        data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                                        data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
                                        <img src={window.location.origin + "/assets/media/avatars/blank.png"} alt="metronic" />
                                    </div>
                                    <div className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                                        data-kt-menu="true">
                                        <div className="menu-item px-3">
                                            <div className="menu-content d-flex align-items-center px-3">
                                                <div className="symbol symbol-50px me-5">
                                                    <img alt="Logo" src={window.location.origin + "/assets/media/avatars/blank.png"} />
                                                </div>
                                                <div className="d-flex flex-column">
                                                    <div className="fw-bolder d-flex align-items-center fs-5">{this.props.admin && this.props.admin.name ? this.props.admin.name : ' - '}</div>
                                                    <div className="fw-bold text-muted fs-7">{this.props.admin && this.props.admin.email ? this.props.admin.email : ' - '}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="separator my-2"></div>
                                        <div className="menu-item px-5">
                                            <div onClick={() => {
                                                Helper.reactToDom(
                                                    window.$('body'),
                                                    <ChangePassword />
                                                )
                                                // alert('change pwd')
                                            }}
                                                style={{ cursor: 'pointer' }}
                                                className="menu-link px-5">Change Password</div>
                                        </div>
                                        <div className="separator my-2"></div>
                                        <div className="menu-item px-5">
                                            <div onClick={() => { this.logout() }}
                                                style={{ cursor: 'pointer' }}
                                                className="menu-link px-5">Sign Out</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}