import React from 'react';
const api_host = "http://localhost:5001/siam-card-game/asia-southeast1/app";

class App extends React.Component {
  componentDidMount() {
    this.getUser();
  }

  getUser() {
    window.$.ajax({
      method: 'GET',
      url: `${api_host}/user`,
      data: { user_id: "16824" },
      success: (res) => {
        console.log(res);
      },
      error: () => {

      }
    })
  }

  login() {
    window.$.ajax({
      method: 'POST',
      url: `${api_host}/user/login`,
      data: { guest_id: 1},
      success: (res) => {
        console.log(res);
      },
      error: () => {

      }
    })
  }


  friendRq() {
    window.$.ajax({
      method: 'POST',
      url: `${api_host}/user/friend/send_request`,
      data: { receive_user_id: "16824" , send_user_id: '16900'},
      success: (res) => {
        console.log(res);
      },
      error: () => {

      }
    })
  }

  friendAccept() {
    window.$.ajax({
      method: 'POST',
      url: `${api_host}/user/friend/accept_request`,
      data: { user_id: "16824" , friend_id: '16900'},
      success: (res) => {
        console.log(res);
      },
      error: () => {

      }
    })
  }

  friendList() {
    window.$.ajax({
      method: 'GET',
      url: `${api_host}/user/friend/list`,
      data: { user_id: "16824"},
      success: (res) => {
        console.log(res);
      },
      error: () => {

      }
    })
  }

  changeName() {
    window.$.ajax({
      method: 'POST',
      url: `${api_host}/user/profile/change_name`,
      data: { user_id: "16824" , user_name: 'Test1'},
      success: (res) => {
        console.log(res);
      },
      error: () => {

      }
    })
  }
  
  render() {
    return (
      <div className="App">
        <div style={{marginBottom: 10}}>
          <button type="button" onClick={()=> {this.friendRq()}}>Friend Request</button>
        </div>
        <div style={{marginBottom: 10}}>
          <button type="button" onClick={()=> {this.friendAccept()}}>Accept Friend Request</button>
        </div>
        <div style={{marginBottom: 10}}>
          <button type="button" onClick={()=> {this.friendList()}}>Friend List</button>
        </div>
        <div style={{marginBottom: 10}}>
          <button type="button" onClick={()=> {this.changeName()}}>Change Name</button>
        </div>
      </div>
    );
  }
}

export default App;
