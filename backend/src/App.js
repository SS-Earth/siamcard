import React from 'react';
import AsideMenu from './Include/AsideMenu';
import Header from './Include/Header';
import { Router } from "@reach/router";
import Helper from './Helper';
import TestApp from './TestApp';
import Login from './Auth/Login'
import Dashboard from './Page/Dashboard';
import User from './Page/User';
import UserView from './Page/UserView';
import Account from './Page/Account';
import Logs from './Page/Logs';
let is_emulators = false;
if (window.location.origin.indexOf('localhost:') !== -1) {
  is_emulators = true;
}
window.api_host = "https://asia-southeast1-siam-card-game.cloudfunctions.net/app";
if (is_emulators) {
  window.api_host = "http://localhost:5001/siam-card-game/asia-southeast1/app";
}
window.secretkey = "siamcard.backend"
export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      is_loading: true,
      is_logged_in: false,
      admin: false,
    }
  }

  componentDidMount() {
    this.loginCheck();
  }

  loginCheck() {
    let token = Helper.getCookie('login_token');
    let state = {
      is_loading: false,
    };
    if (token) {
      state.is_logged_in = true;
      let admin = Helper.decrypt(token);
      state.admin = JSON.parse(admin);
    }
    if (!token) {
      state.is_logged_in = false
    }
    this.setState(state)
  }


  render() {
    if (this.state.is_loading) {
      return (<>Loading...</>)
    }
    if (!this.state.is_logged_in) {
      return <Login />
    }

    return (
      <div className="page d-flex flex-row flex-column-fluid">
        <AsideMenu admin={this.state.admin}/>
        <div className="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
          <Header admin={this.state.admin}/>
          <div className="content d-flex flex-column flex-column-fluid" id="kt_content">
            <Router>
              <Dashboard path="/" admin={this.state.admin}/>
              <User path="/user" admin={this.state.admin}/>
              <UserView path="/user/:uid" admin={this.state.admin}/>
              <Account path="/account" admin={this.state.admin}/>
              <Logs path="/logs" admin={this.state.admin}/>
              <TestApp path="/test-app" admin={this.state.admin}/>
            </Router>
          </div>
        </div>
      </div>
    );
  }
}


