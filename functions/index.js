const functions = require("firebase-functions");
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();
const storage = admin.storage();
const auth = admin.auth();
var cors = require('cors')
var CryptoJS = require("crypto-js");
var Hex = require("crypto-js/enc-hex");
var HmacSHA256 = require("crypto-js/hmac-sha256");
const secretkey = "siamcard.backend"
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.region('asia-southeast1').https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
const express = require('express')
const app = express();
const asyncHandler = require('express-async-handler');

app.use(cors());
async function setLog(object) {
  let created = new Date().getTime();
  await db.collection(`logs`).add({ ...object, created: created });
}

function encrypt(message) {
  var encryptedMessage = CryptoJS.AES.encrypt(message, secretkey);
  return encryptedMessage.toString();
}
function decrypt(message) {
  var decryptedBytes = CryptoJS.AES.decrypt(message, secretkey);
  var decryptedMessage = decryptedBytes.toString(CryptoJS.enc.Utf8);
  return decryptedMessage;
}
async function randomUserId() {
  let last_user_id = 16801;
  let last_user = await db.collection(`user_data`).orderBy('create_date', 'desc').limit(1).get();
  if (!last_user.empty) {
    last_user_id = parseFloat(last_user.docs[0].data().user_id)
  }
  let random_number = Math.floor(Math.random() * 100) + 1;
  let user_id = last_user_id + random_number;
  return user_id.toString();
}

function setUser(user) {
  let return_user = { ...user };
  delete return_user.social_apple_id;
  delete return_user.social_facebook_id;
  delete return_user.social_gmail_id;
  return return_user;
}
function setAdmin(admin) {
  let return_admin = { ...admin };
  delete return_admin.password;
  // delete return_admin.is_admin;
  delete return_admin.class;
  delete return_admin.token;
  return return_admin;
}

function setPagination(count_all_items, limit, now_page) {
  let pages = Math.ceil(parseFloat(count_all_items) / parseFloat(limit));
  if (isNaN(pages)) {
    pages = 0;
  }
  let obj = {};
  obj.pages = pages;
  obj.now_page = parseFloat(now_page);
  obj.count_all_items = parseFloat(count_all_items);
  obj.limit = parseFloat(limit)
  return obj;
}

function prepareDataLimit(data, limit, page, options) {
  let return_data = [];
  data = data.filter((__data) => { return __data });
  if (options && options.mergePage) {
    return_data = data.filter((__data, __data_i) => {
      return __data_i < (limit * page)
    })
  }
  if (!options) {
    return_data = data.filter((__data, __data_i) => {
      return __data_i < (limit * page)
        && __data_i > (((limit * page) - limit) - 1)
    })
  }
  return return_data;
}
function setOffset(limit, page) {
  return ((parseFloat(limit) * parseFloat(page)) - parseFloat(limit))
}

function updateEvent(event, userData) {
  let _return = {
    played_dummy: userData.played_dummy,
    played_pokdeng: userData.played_pokdeng,
    played_slave: userData.played_slave,
  };
  if (event.toLowerCase() === 'pokdeng') {
    _return.played_pokdeng = parseFloat(userData.played_pokdeng) + 1;
  }
  if (event.toLowerCase() === 'dummy') {
    _return.played_dummy = parseFloat(userData.played_dummy) + 1;
  }
  if (event.toLowerCase() === 'slave') {
    _return.played_slave = parseFloat(userData.played_slave) + 1;
  }
  if (event.toLowerCase() === 'iap') { }
  if (event.toLowerCase() === 'daily') { }
  return _return;
}


app.post('/user/login', asyncHandler(async (req, res) => {
  const qs = req.body;
  let now_date = new Date();
  // check guest id
  let ref = await db.collection(`user_data`).where('guest_id', '==', qs.guest_id).get();
  let userData = {};
  if (!ref.empty) {
    let data = ref.docs[0];
    userData = data.data();
    userData.id = data.id;
  } else {
    let user_id = await randomUserId();
    let defaultData = {
      user_id: user_id,
      user_name: 'Guest',
      user_coin: 0,
      user_chip: 0,
      create_date: now_date.getTime(),
      social_facebook_id: null,
      social_gmail_id: null,
      social_apple_id: null,
      guest_id: qs.guest_id,
      played_pokdeng: 0,
      played_dummy: 0,
      played_slave: 0,
      total_highest_coin: 0,
      total_highest_chip: 0,
      user_status: 'active',
    }
    let create_ref = await db.collection(`user_data`).doc(user_id).set({ ...defaultData });
    userData = defaultData;
    userData.id = create_ref.id;
  }
  let responseUser = setUser(userData);
  return res.status(200).json({ message: 'login success', is_success: true, user: responseUser });
}))
app.get('/user', asyncHandler(async (req, res) => {
  const { user_id, page, limit, keyword, is_get_friend } = req.query;
  let users = [];
  let _limit = 10;
  if (limit) {
    _limit = limit;
  }
  let _page = 1;
  if (_page) {
    _page = page;
  }
  let offset = setOffset(_limit, _page);
  if (user_id) {
    let userData = {};
    let ref = await db.collection(`user_data`).doc(user_id).get();
    if (!ref.exists) {
      return res.status(200).json({ message: 'no user', is_success: false });
    }
    let data = ref;
    userData = data.data();
    userData.id = data.id;

    let responseUser = setUser(userData);
    let fri_ref = await db.collection(`user_data`).doc(user_id).collection('friends').select().get();
    responseUser.friends_count = fri_ref.size;

    let responseData = { message: 'get user success', is_success: true, user: responseUser }
    if (page && limit) {
      let logRef = await db.collection(`logs`).where(`user_id`, `==`, user_id).offset(offset).limit(parseFloat(_limit)).orderBy(`created`, `desc`).get();
      let logs = [];
      if (!logRef.empty) {
        for (const doc of logRef.docs) {
          let log = doc.data();
          log.id = doc.id;
          logs.push(log);
        }
      }
      // logs = prepareDataLimit(logs, _limit, _page)
      responseData.logs = logs;
      let countAll = 0;
      let countAllRef = db.collection('logs').where(`user_id`, `==`, user_id);
      // if (keyword) {
      //   countAllRef = countAllRef.where(`user_id`, `==`, keyword)
      // }
      countAllRef = await countAllRef.select().get()
      countAll = countAllRef.size;
      let pagination = setPagination(countAll, _limit, _page);
      responseData.pagination = pagination;
    }

    return res.status(200).json(responseData)
  } else {
    let ref = db.collection(`user_data`)
    if (keyword) {
      ref = ref.where(`user_id`, `==`, keyword).offset(offset).limit(parseFloat(_limit))
    }
    if (!keyword) {
      ref = ref.offset(offset).limit(parseFloat(_limit)).orderBy(`user_id`, `desc`)
    }
    ref = await ref.get();
    if (!ref.empty) {
      for (const doc of ref.docs) {
        let user = doc.data();
        user.id = doc.id;
        let pushUser = setUser(user);
        users.push(pushUser);
      }
    }
    let countAllRef = await db.collection('user_data');
    if (keyword) {
      countAllRef = countAllRef.where(`user_id`, `==`, keyword)
    }
    countAllRef = await countAllRef.select().get();
    let countAll = countAllRef.size;
    let pagination = setPagination(countAll, _limit, _page);
    return res.status(200).json({ message: 'get users success', is_success: true, users: users, pagination: pagination })

  }
}))

app.post('/user/chip/increase', asyncHandler(async (req, res) => {
  const { amount, user_id, event } = req.body;
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(400).json({ message: 'no user', is_success: false });
  }
  if (!event) {
    return res.status(400).json({ message: 'no event', is_success: false });
  }
  if (parseFloat(amount) === 0) {
    return res.status(200).json({ message: 'no chip amount', is_success: false })
  }
  let userData = userRef.data();
  let update_event = updateEvent(event, userData)
  await db.collection(`user_data`).doc(userRef.id).update({
    user_chip: userData.user_chip + parseFloat(amount),
    ...update_event
  });
  setLog({
    amount: parseFloat(amount),
    total: userData.user_chip + parseFloat(amount),
    user: { ...userData, ...update_event },
    user_id: user_id,
    type: "chip_increase",
    event: event
  })
  return res.status(200).json({ message: 'add chip success', is_success: true })
}))
app.post('/user/chip/decrease', asyncHandler(async (req, res) => {
  const { amount, user_id, event } = req.body;
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(400).json({ message: 'no user', is_success: false });
  }
  if (!event) {
    return res.status(400).json({ message: 'no event', is_success: false });
  }
  if (parseFloat(amount) === 0) {
    return res.status(200).json({ message: 'no chip amount', is_success: false })
  }
  let userData = userRef.data();
  let setAmount = userData.user_chip - parseFloat(amount);
  if (setAmount < 0) {
    setAmount = 0;
  }
  let update_event = updateEvent(event, userData)
  await db.collection(`user_data`).doc(userRef.id).update({ user_chip: setAmount, ...update_event });
  setLog({
    amount: parseFloat(amount),
    total: setAmount,
    user: { ...userData, ...update_event },
    user_id: user_id,
    type: "chip_decrease",
    event: event
  })
  return res.status(200).json({ message: 'add chip success', is_success: true })
}))
app.post('/user/coin/increase', asyncHandler(async (req, res) => {
  const { amount, user_id, event } = req.body;
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(400).json({ message: 'no user', is_success: false });
  }
  if (!event) {
    return res.status(400).json({ message: 'no event', is_success: false });
  }
  if (parseFloat(amount) === 0) {
    return res.status(200).json({ message: 'no coin amount', is_success: false })
  }
  let userData = userRef.data();
  let update_event = updateEvent(event, userData)
  await db.collection(`user_data`).doc(userRef.id).update({ user_coin: userData.user_coin + parseFloat(amount), ...update_event });
  setLog({
    amount: parseFloat(amount),
    total: userData.user_coin + parseFloat(amount),
    user: { ...userData, ...update_event },
    user_id: user_id,
    type: "coin_increase",
    event: event
  })
  return res.status(200).json({ message: 'add coin success', is_success: true })
}))
app.post('/user/coin/decrease', asyncHandler(async (req, res) => {
  const { amount, user_id, event } = req.body;
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(400).json({ message: 'no user', is_success: false });
  }
  if (!event) {
    return res.status(400).json({ message: 'no event', is_success: false });
  }
  if (parseFloat(amount) === 0) {
    return res.status(200).json({ message: 'no coin amount', is_success: false })
  }
  let userData = userRef.data();
  let setAmount = userData.user_coin - parseFloat(amount);
  if (setAmount < 0) {
    setAmount = 0;
  }
  let update_event = updateEvent(event, userData)
  await db.collection(`user_data`).doc(userRef.id).update({ user_coin: setAmount, ...update_event });
  setLog({
    amount: parseFloat(amount),
    total: setAmount,
    user: { ...userData, ...update_event },
    user_id: user_id,
    type: "coin_decrease",
    event: event
  })
  return res.status(200).json({ message: 'add coin success', is_success: true })
}))

app.post('/event/daily', asyncHandler(async (req, res) => {
  const { user_id } = req.body;
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(400).json({ message: 'no user', is_success: false });
  }
  let userData = userRef.data();

  let now = new Date();
  let claimdaily = `${now.getDate()}-${now.getMonth()}-${now.getFullYear()}`;
  if (userData.claimdaily && userData.claimdaily === claimdaily) {
    return res.status(400).json({ message: 'dialy claimed', is_success: false });
  }
  let random = Math.floor(Math.random() * 50000) + 5000;
  random = random.toString();
  random = random.slice(0, -3);
  random = random + '000';
  random = parseFloat(random);
  await db.collection('user_data').doc(userRef.id).update({ claimdaily: claimdaily });
  return res.status(200).json({ message: 'event dialy success', is_success: true, userData: userData, coin: random })
}))

app.post('/user/friend/send_request', asyncHandler(async (req, res) => {
  const { send_user_id, receive_user_id } = req.body;
  let sendRef = await db.collection(`user_data`).doc(send_user_id).get();
  if (!sendRef.exists) {
    return res.status(200).json({ message: 'no sender', is_success: false });
  }
  let sendReceiveFri = await db.collection(`user_data`).doc(send_user_id).collection(`friends`).doc(receive_user_id).get();
  if (sendReceiveFri.exists) {
    let message = '';
    if (sendReceiveFri.data().status === 3) {
      message = 'send friend request already';
    }
    if (sendReceiveFri.data().status === 2) {
      message = 'friend has already sent you a request';
    }
    if (sendReceiveFri.data().status === 1) {
      message = 'we are friend now';
    }
    if (sendReceiveFri.data().status === 4) {
      message = 'friend request canceled';
    }
    return res.status(200).json({ message: message, is_success: false });
  }

  let receiveRef = await db.collection(`user_data`).doc(receive_user_id).get();
  if (!receiveRef.exists) {
    return res.status(200).json({ message: 'no receiver', is_success: false });
  }

  await db.collection(`user_data`).doc(send_user_id).collection(`friends`).doc(receive_user_id).set({ friend_id: receive_user_id, status: 3 });
  await db.collection(`user_data`).doc(receive_user_id).collection(`friends`).doc(send_user_id).set({ friend_id: send_user_id, status: 2 });
  return res.status(200).json({ message: 'send request complete', is_success: true });
}))

app.post('/user/friend/accept_request', asyncHandler(async (req, res) => {
  const { user_id, friend_id } = req.body;
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(200).json({ message: 'no user', is_success: false });
  }
  let friendRef = await db.collection(`user_data`).doc(friend_id).get();
  if (!friendRef.exists) {
    return res.status(200).json({ message: 'no friend', is_success: false });
  }
  await db.collection(`user_data`).doc(user_id).collection(`friends`).doc(friend_id).update({ status: 1 });
  await db.collection(`user_data`).doc(friend_id).collection(`friends`).doc(user_id).update({ status: 1 });
  return res.status(200).json({ message: 'send request complete', is_success: true });
}))

app.get('/user/friend/list', asyncHandler(async (req, res) => {
  const { user_id, status } = req.query;
  let friendStatus = 'all';
  if (status) { friendStatus = status };
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(200).json({ message: 'no user', is_success: false });
  }
  let ref = db.collection(`user_data`).doc(user_id).collection(`friends`)
  if (friendStatus !== 'all') {
    ref = ref.where(`status`, `==`, friendStatus)
  }
  let friendRef = await ref.get();
  let friends = [];
  let friendPrepareData = [];
  if (!friendRef.empty) {
    for (const doc of friendRef.docs) {
      let friend = { ...doc.data() };
      friend.id = doc.id;
      let get_friend_user = db.collection(`user_data`).doc(friend.friend_id).get();
      friendPrepareData.push(get_friend_user);
      friends.push(friend);
    }
  }
  let prepare = await Promise.all(friendPrepareData);
  let i = 0;
  for (const doc of prepare) {
    let frienddata = doc.data();
    friends[i].user = setUser(frienddata);
    i++;
  }
  return res.status(200).json({ message: 'get friend success', is_success: true, friends: friends });
}))

app.post('/user/profile/change_name', asyncHandler(async (req, res) => {
  const { user_id, user_name } = req.body;
  let userRef = await db.collection(`user_data`).doc(user_id).get();
  if (!userRef.exists) {
    return res.status(200).json({ message: 'no user', is_success: false });
  }
  await db.collection(`user_data`).doc(user_id).update({ user_name: user_name });
  let ref = await db.collection(`user_data`).doc(user_id).get();
  let userData = setUser(ref.data());
  return res.status(200).json({ message: 'change user name success', is_success: true, user: userData });
}))


//LOGS
app.get('/logs', asyncHandler(async (req, res) => {
  const { page, limit, keyword } = req.query;
  let logs = [];
  let _limit = 10;
  if (limit) {
    _limit = limit;
  }
  let _page = 1;
  if (_page) {
    _page = page;
  }
  let offset = setOffset(_limit, _page);
  // console.log(_page, _limit, offset)
  let ref = db.collection(`logs`);
  if (keyword) {
    ref = ref.where(`user_id`, `==`, keyword)
  }
  ref = ref.offset(offset).limit(parseFloat(_limit));
  ref = ref.orderBy(`created`, `desc`)
  ref = await ref.get();
  if (!ref.empty) {
    for (const doc of ref.docs) {
      let log = doc.data();
      log.id = doc.id;
      logs.push(log)
    }
  }
  // logs = prepareDataLimit(logs, _limit, _page)

  let countAll = 0;
  let countAllRef = db.collection('logs');
  if (keyword) {
    countAllRef = countAllRef.where(`user_id`, `==`, keyword)
  }
  countAllRef = await countAllRef.select().get()
  countAll = countAllRef.size;
  let pagination = setPagination(countAll, _limit, _page);
  return res.status(200).json({ message: 'get logs success', is_success: true, logs: logs, pagination: pagination })

}))
// ADMIN
app.get('/admin', asyncHandler(async (req, res) => {
  const { email, page, limit, keyword } = req.query;
  let admins = [];
  if (email) {
    let adminData = {};
    let ref = await db.collection(`admin`).where(`email`, `==`, email).get();
    if (ref.empty) {
      return res.status(200).json({ message: 'no account', is_success: false });
    }
    let data = ref.docs[0];
    adminData = data.data();
    adminData.id = data.id;
    let responseAdmin = setAdmin(adminData);
    return res.status(200).json({ message: 'get user success', is_success: true, admin: responseAdmin })
  } else {
    let _limit = 10;
    if (limit) {
      _limit = limit;
    }
    let _page = 1;
    if (_page) {
      _page = page;
    }
    let offset = setOffset(_limit, _page);
    let ref = await db.collection(`admin`);
    if (!keyword) {
      ref = ref.offset(offset).limit(parseFloat(_limit));
    }
    ref = await ref.get()
    if (!ref.empty) {
      for (const doc of ref.docs) {
        let admin = doc.data();
        admin.id = doc.id;
        let pushAdmin = setAdmin(admin);
        if (!keyword
          || (keyword
            && (admin.name.indexOf(keyword) !== -1
              || admin.email.indexOf(keyword) !== -1))
        ) {
          admins.push(pushAdmin);
        }
      }

      admins = prepareDataLimit(admins, _limit, _page)

      let countAll = 0;
      if (!keyword) {
        let countAllRef = await db.collection('admin').select().get();
        countAll = countAllRef.size;
      }
      if (keyword) {
        countAll = admins.length;
      }
      let pagination = setPagination(countAll, _limit, _page);
      return res.status(200).json({ message: 'get users success', is_success: true, admins: admins, pagination: pagination })
    }
  }

  return res.status(200).json({ message: 'create admin success', is_success: true, data: data });
}))
app.post('/admin', asyncHandler(async (req, res) => {
  const data = req.body;
  if (!data || !data.email || !data.name || !data.password) {
    return res.status(200).json({ message: 'Please Complete Information', is_success: false });
  }
  let ref = await db.collection(`admin`).where(`email`, `==`, data.email).get();
  if (!ref.empty) {
    return res.status(200).json({ message: 'Email has been used', is_success: false });
  }
  let admin_data = { ...data };
  admin_data.class = 'admin';
  await db.collection('admin').add(admin_data);
  return res.status(200).json({ message: 'create admin success', is_success: true, data: data });
}))
app.delete('/admin', asyncHandler(async (req, res) => {
  const { id } = req.body;
  if (!id) {
    return res.status(200).json({ message: 'No Account', is_success: false });
  }
  let ref = await db.collection(`admin`).doc(id).get();
  if (!ref.exists) {
    return res.status(200).json({ message: 'No Account', is_success: false });
  }

  await db.collection(`admin`).doc(id).delete();
  return res.status(200).json({ message: 'delete admin success', is_success: true });

}))
app.put('/admin-change-password', asyncHandler(async (req, res) => {
  const data = req.body;
  const token = req.headers['siamcard-token'];
  if (!data.old_password || !data.new_password || !data.confirm_password) {
    return res.status(200).json({
      message: 'No information',
      is_success: false,
    });
  }
  if (data.new_password !== data.confirm_password) {
    return res.status(200).json({
      message: 'Confrim password not match',
      is_success: false,
    });
  }

  let adminGetRef = await db.collection('admin').where('token', '==', token).get();
  if (adminGetRef.empty) {
    return res.status(200).json({
      message: 'No User',
      is_success: false,
    });
  }
  let adminRef = adminGetRef.docs[0]
  let adminData = adminRef.data();
  adminData.id = adminRef.id;

  if (adminData.password != data.old_pwd_encypt) {
    return res.status(200).json({
      message: 'Old password not match',
      is_success: false,
    });
  }

  adminRef.ref.update({ password: data.new_pwd_encypt })

  return res.status(200).json({
    message: 'Password changed',
    is_success: true,
    data: data,
    token: token
  });
}))
//LOGIN
app.get('/account', asyncHandler(async (req, res) => {
  const token = req.headers['siamcard-token'];
  let decode = decrypt(token);
  let decode_admin = JSON.parse(decode);
  let adminRef = await db.collection(`admin`).doc(decode_admin.id).get();
  if (!adminRef.exists) {
    return res.status(200).json({
      message: 'No Account',
      is_success: false,
    });
  }
  let admin = setAdmin(adminRef.data());
  return res.status(200).json({
    message: 'Get Account Success',
    is_success: true,
    admin: admin
  });
}))
app.post('/login', asyncHandler(async (req, res) => {
  const data = req.body;
  let pwd = Hex.stringify(HmacSHA256(data.password, secretkey));
  let ref = await db.collection('admin').where('email', '==', data.email).get();
  if (ref.empty) {
    return res.status(200).json({ message: 'Email or Password not correct.', is_success: false });
  }
  let adminRef = ref.docs[0];
  let adminData = adminRef.data();
  adminData.id = adminRef.id;
  if (adminData.password !== pwd) {
    return res.status(200).json({ message: 'Email or Password not correct.', is_success: false });
  }
  // let token = Hex.stringify(HmacSHA256(adminData.id, secretkey));
  let token = encrypt(JSON.stringify(setAdmin(adminData)));
  adminRef.ref.update({ token: token })
  return res.status(200).json({
    message: 'Login Success',
    is_success: true,
    token: token
  });
}))

app.post('/test', asyncHandler(async (req, res) => {
  const token = req.headers['siamcard-token'];

  let token_decrypt = decrypt(token);
  return res.status(200).json({
    message: ' Success',
    is_success: true,
    token: token || "0",
    decrypt: token_decrypt,
  });
}))
// app.listen(3000)
exports.app = functions.region('asia-southeast1').https.onRequest(app);